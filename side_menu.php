<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Document</title>

  <style>
    *{
        overflow-x: hidden;
    }
    .navbar{
      background-color: #00bfa5;
    }
    .side-menu a{
        display: block;
        text-decoration: none;
        color: black;
        padding: 4%;     
    }
    ul{
        list-style-type: none;
        margin: 0px;
        padding: 0px;
        width: 100%; 
    }
    li a:hover{
        background: gainsboro;
    }
    .fa-cog{
        float:left;
        margin: 5px;
    }
    .side-icon{
        padding-right: 20px;
        padding-left: 20px;
    }
    
  </style>
</head>
<body>
 <!--NAVBAR  -->
<nav class="navbar navbar-light text-white">
   <div class="row" style="width: 100%;">
       <div class="col-sm-4 col-md-4 col-lg-3">
            <a class="navbar-brand"><i class="fa fa-bars pl-4" aria-hidden="true"></i></a>
            <a class="navbar-brand"><h4>Navbar</h4></a>
       </div>
       <div class="col-sm-8 col-md-8 col-lg-9">
            <a class="navbar-brand"><h4>Page Name</h4></a>
       </div>
   </div>  
</nav>

<!-- MENU MASTER -->
<div class="row">
    <div class="col-lg-3 col-sm-4 col-12">
       <ul>
           <li class="side-menu">
               <a href=""> <i class="fa fa-money side-icon "></i>Struk dan Biaya</a>           
           </li>
       </ul>    
    </div>
</div>

</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
