<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Document</title>

  <style>
    *{
        overflow-x: hidden;
    }
    .navbar{
      background-color: #00bfa5;
    }
    .side-menu a{
        display: block;
        text-decoration: none;
        color: black;
        padding: 4%;     
    }
    ul{
        list-style-type: none;
        margin: 0px;
        padding: 0px;
        width: 100%; 
    }
    li a:hover{
        background: gainsboro;
    }
    .btn{
        border:1px solid #00bfa5;
        width: 100%;
        color:#00bfa5;
    }
    .btn:hover{
        background: #00bfa5;
        color: #fff;
        
    }
    .fa-cog{
        float:left;
        margin: 5px;
    }
    .card{
        width: 100%;
        height: 450px;
        background: gainsboro;
    }
    #paper{
        background: white;
        width:80%;
        margin-left: 10%;
        margin-right: 10%;
    }
    h5{
        color:#00bfa5;
    }
    .text{
        width: 80%;
        border: none;
        border-bottom: 1px solid gainsboro;
    }
    span{
        font-size: 12px;
        color:gray;
    }
    #no-urut{
        width: 75%;
    }
    .side-icon{
        padding-right: 20px;
        padding-left: 20px;
    }
    
    
  
  </style>
</head>
<body>
 <!--NAVBAR  -->
<nav class="navbar navbar-light text-white">
   <div class="row" style="width: 100%;">
       <div class="col-sm-4 col-md-4 col-lg-3">
            <a class="navbar-brand"><i class="fa fa-bars pl-4" aria-hidden="true"></i></a>
            <a class="navbar-brand"><h4>Pengaturan</h4></a>
       </div>
       <div class="col-sm-8 col-md-8 col-lg-9">
            <a class="navbar-brand"><h4>Struk dan Biaya</h4></a>
       </div>
   </div>  
</nav>

<!-- CONTENT -->
<div class="row">
    <!-- SIDE MENU -->
    <div class="col-lg-3 col-sm-4 col-12">
       <ul>
           <li class="side-menu">
               <a href=""> <i class="fa fa-money side-icon "></i>Struk dan Biaya</a>           
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-print side-icon "></i>Perangkat</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-credit-card side-icon "></i>Pembayaran Non-Tunai</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-coffee side-icon "></i>Info Outlet</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-user side-icon "></i>Password dan PIN</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-shopping-basket side-icon "></i>Promo</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-bell side-icon "></i>Daftar Notifikasi</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-circle-o-notch side-icon "></i>Sinkronisasi</a>
           </li>
           <li class="side-menu">
               <a href=""><i class="fa fa-cog side-icon "></i>Lainnya</a>
           </li>
       </ul>
    </div>
    <!-- TAMPILAN STRUK -->
    <div class=" col-sm-8 col-lg-9 col-12 mt-3">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn font-weight-bold">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    ATUR TAMPILAN STRUK
                </button>
                <div class="card mt-3">
                    <div class="card" id="paper"></div>
                </div>
                <p>*Simulasi</p>
            </div>

            <!-- STRUK SETTINGS -->
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h5>STRUK KASIR</h3>
                <form action="" method="post">

                    <div class="form-group">
                        <span>HEADER FOOTER STRUK</span>
                        <div class="row">
                            <input type="text" name="" id="" class="text">
                            <a href="#">></a>
                        </div>
                    </div>

                    <div class="form-group">
                    <span>JUMLAH CETAK STRUK</span>
                        <div class="row">
                            <input type="text" name="" id="" class="text">
                            <a href="#">></a>
                        </div>
                    </div>

                    <div class="form-group">
                    <span>NOMOR URUT</span>
                        <div class="row">
                            <input type="text" name="" id="no-urut" class="text">
                            <span class="ml-1 mr-1">OFF</span>
                            <a href="#">></a>
                        </div>
                    </div>

                    <h5>BIAYA</h3>
                    <div class="form-group">
                    <span>PAJAK - SETELAH DISKON</span>
                        <div class="row">
                            <input type="text" name="" id="no-urut" class="text">
                            <span class="ml-1 mr-1">OFF</span>
                            <a href="#">></a>
                        </div>
                    </div>

                    <div class="form-group">
                    <span>SERVICE CHARGE - TIDAK KENA PAJAK </span>
                        <div class="row">
                            <input type="text" name="" id="no-urut" class="text">
                            <span class="ml-1 mr-1">OFF</span>
                            <a href="#">></a>
                        </div>
                    </div>

                    <div class="form-group">
                    <span>PEMBULATAN PECAHAN</span>
                        <div class="row">
                            <input type="text" name="" id="no-urut" class="text">
                            <span class="ml-1 mr-1">OFF</span>
                            <a href="#">></a>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
        
    </div>
</div>

</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
