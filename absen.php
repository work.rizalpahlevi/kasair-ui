<?php 
    date_default_timezone_set('Asia/Jakarta');
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Absen</title>
    <style>
    *{
        overflow-x: hidden;
        
    }
    
    .navbar{
      background-color: #00bfa5;
    }
    .time{
        z-index: 2; 
        position: absolute;
        color:white;
        margin-left:3%;
        margin-top:10px;
    }
    h1,h2,h3,h4,h5,h6{
        padding:2px;
    }
    .date{
        padding:0px;
        margin:0px;
        overflow: hidden;
    } 
    .video{
        position:relative;
    }
    .absence-time{
        padding:1%;
    }
    span{
        font-size: 15px;
    }
    p{
        font-size: 35px;
        color:#00bfa5;
    }
    #staff{
        width: 80%;
        border:none;
        border-bottom: 1px solid gray;
        background: gainsboro;
    }
    
    #password{
        width: 80%;
        border:none;
        border-bottom: 1px solid gray;
        font-size: 30px;
        letter-spacing: 20px;
        background: gainsboro;
    }
    .num-pad{
        width: 100%;
        padding:30%;
    }
    .num-pad:hover{
        background: #00bfa5;
    }
    .log-btn{
        width: 100%;
        background: gray;
        margin: 0px;
        border-radius: 0px;
        padding: 10%;
        
    }
    .log-btn:hover{
        background: #00bfa5;
    }
    .button-log{
        padding: 0px;
    } 
    .num-pad-row{
        margin-left: 15%;
    }
    .input{
        margin-left: 15%;
    }
    .button-log-row{
        bottom: 0;
    }
    .menu{
        background: gainsboro;
        padding: 0px;
    }
    .col-video{
        padding: 0px;
    }
    .content{
        overflow: hidden;
    }
    .btn-light,.btn-light:hover{
        color: #00bfa5;
    }
    
  </style>
</head>
<body>

<!-- NAVBAR  -->
<nav class="navbar navbar-light text-white">
   <div class="row" style="width: 100%;">
       <div class="col-sm-4 col-md-4 col-lg-3">
            <a class="navbar-brand"><i class="fa fa-bars pl-4" aria-hidden="true"></i></a>
            <a class="navbar-brand"><h4>Navbar</h4></a>
       </div>
       <div class="col-sm-8 col-md-8 col-lg-9 text-right">
            <a class="navbar-brand"><button class="btn btn-light">DAFTAR ABSENSI</button></a>
       </div>
   </div>  
</nav>

<!-- CAMERA SIDE -->
<div class="row content">
    <div class="col-md-6 col-video">
        <div class="video">
            <div class="time">
                <h4 class="date"><?=date('d-m-Y');?></h4>   
                <h1 class="date"><?=date('H:i');?></h1> 
            </div>
            <video autoplay="true" id="video-webcam" style="width:100%; "></video>
            <div class="row">
                <div class="absence-time col-md-6 col-sm-6 text-center">
                    <span>JAM MASUK:</span>
                    <p>--:--</p>
                </div>
                <div class="absence-time col-md-6 col-sm-6 text-center">
                    <span>JAM PULANG:</span>
                    <p>--:--</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Menu Staff-->
    <div class="menu col-md-6">
        <form action="" method="post" class="mt-3">
            <div class="input form-group">
                <label for="staff" style="width: 100%;">PILIH STAFF</label>
                <select name="" id="staff">
                    <option value="">Choose</option>
                </select>
            </div>

            <div class="input form-group">
                <label for="password">MASUKKAN PASSWORD</label>
                <input type="text" name=""  id="password" class="text-center">
            </div>

        <!-- NUM Pad -->
        <div class="form-group">
            <div class="row num-pad-row mt-3 mb-3">
                <div class="col-sm-3 col-md-3 col-3">
                    <button class="num-pad btn">1</button>
                    <button class="num-pad btn">2</button>
                    <button class="num-pad btn">3</button>
                </div>
                <div class="col-sm-3 col-md-3 col-3">
                    <button class="num-pad btn">4</button>
                    <button class="num-pad btn">5</button>
                    <button class="num-pad btn">6</button>
                </div>
                <div class="col-sm-3 col-md-3 col-3">
                    <button class="num-pad btn">7</button>
                    <button class="num-pad btn">8</button>
                    <button class="num-pad btn">9</button>
                </div>
            </div>
        </div>
        <!-- BUTTON ADD JAM MASUK DAN PULANG -->
        <div class="row button-log-row">
                <div class="button-log col-sm-6 col-md-6 col-lg-6 col-6">
                    <button class="log-btn btn">MASUK</button>
                </div>
                <div class="button-log col-sm-6 col-md-6 col-lg-6 col-6">
                    <button class="log-btn btn">PULANG</button>
                </div>
            </div>
    </form>
</div>
</div>


</body>
</html>
<script type="text/javascript">
    // seleksi elemen video
    var video = document.querySelector("#video-webcam");

    // minta izin user
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

    // jika user memberikan izin
    if (navigator.getUserMedia) {
        // jalankan fungsi handleVideo, dan videoError jika izin ditolak
        navigator.getUserMedia({ video: true }, handleVideo, videoError);
    }

    // fungsi ini akan dieksekusi jika  izin telah diberikan
    function handleVideo(stream) {
        video.srcObject = stream;
    }

    // fungsi ini akan dieksekusi kalau user menolak izin
    function videoError(e) {
        // do something
        alert("Izinkan menggunakan webcam untuk demo!")
    }
</script>